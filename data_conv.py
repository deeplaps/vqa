#import statements
import json
import operator
import os
import torch
from skimage import io, transform
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

#Global Variables
root_data_dir = '/scratch/cse/btech/cs1140425/DL_Course_Data/'
K =1000


def getDataLoader(input):
	file = open(root_data_dir+input,'r')
	datastore = json.load(file)
	return datastore

data = getDataLoader('v2_mscoco_train2014_annotations.json')

# print(len(data['annotations']))
freq_ans ={}
for items in data['annotations']:
	answer = items['multiple_choice_answer']
	if(answer in freq_ans):
		freq_ans[answer] = freq_ans[answer] + 1
	else:
		freq_ans[answer] = 1
# print(len(freq_ans))

answer_ids ={}
count =0
sorted_freq_ans = sorted(freq_ans.items(), key=operator.itemgetter(1),reverse=True)
sorted_freq_ans = sorted_freq_ans[0:K]
# print(sum(i[1] for i in sorted_freq_ans))
for key, value in sorted_freq_ans:
	answer_ids[key] = count
	count = count + 1

maxi =0
train_data = []
Qdata = getDataLoader('v2_OpenEnded_mscoco_train2014_questions.json')
for items in Qdata['questions']:
	if(maxi<len(items['question'].split())):
		maxi = len(items['question'].split())
	train_data.append(items['question'].split())

val_data = []
Qdata = getDataLoader('v2_OpenEnded_mscoco_val2014_questions.json')
for items in Qdata['questions']:
	if(maxi<len(items['question'].split())):
		maxi = len(items['question'].split())
	val_data.append(items['question'].split())

# print(maxi)

word_to_ix = {}

for line in train_data:
	for word in line:
		if word not in word_to_ix:
			word_to_ix[word] = len(word_to_ix)+1

for line in val_data:
	for word in line:
		if word not in word_to_ix:
			word_to_ix[word] = len(word_to_ix)+1

maximum = max(word_to_ix.values())

def prepare_sequence(seq, to_ix):
	idxs =[]
	for w in seq:
		if w not in to_ix:
			idxs.append(randint(0, maximum))
		else:
			idxs.append(to_ix[w])
	return np.array(idxs)

class QuestionDataset(Dataset):
	"""Face Landmarks dataset."""

	def __init__(self, answer_json,question_json, image_dir, transform=None):
		#image_dir will contain the whole path before 0's
		dataload = getDataLoader(answer_json)
		self.data =[]
		for items in dataload['annotations']:
			tmp =[]
			tmp.append(items['image_id'])
			tmp.append(items['question_id'])
			answer = items['multiple_choice_answer']
			if(answer in answer_ids):
				tmp.append(answer_ids[answer])
			else:
				tmp.append(K)
			self.data.append(tmp)
		self.Questions ={}
		Qdata = getDataLoader(question_json)
		for items in Qdata['questions']:
			self.Questions[items['question_id']] = items['question']
		self.image_dir = image_dir
		self.transform = transform

	def __len__(self):
		return len(self.data)

	def __getitem__(self, idx):
		img_name = self.image_dir+str(self.data[idx][0]).zfill(12)+'.jpg'
		image = io.imread(img_name)
		question = prepare_sequence(self.Questions[self.data[idx][1]].split(),word_to_ix)
		question = np.pad(question, (0, maxi-len(question)), 'constant')
		sample = {'image': image, 'question':question ,
		'answer': np.array([self.data[idx][2]])}

		if self.transform:
			sample = self.transform(sample)

		return sample

class Rescale(object):
	def __init__(self, output_size):
		assert isinstance(output_size, (int, tuple))
		self.output_size = output_size

	def __call__(self, sample):
		image, question, answer = sample['image'], sample['question'],sample['answer']

		h, w = image.shape[:2]
		if isinstance(self.output_size, int):
			if h > w:
				new_h, new_w = self.output_size * h / w, self.output_size
			else:
				new_h, new_w = self.output_size, self.output_size * w / h
		else:
			new_h, new_w = self.output_size

		new_h, new_w = int(new_h), int(new_w)

		img = transform.resize(image, (new_h, new_w))

		return {'image': img, 'question': question,'answer':answer}

class RandomCrop(object):
	"""Crop randomly the image in a sample.

	Args:
		output_size (tuple or int): Desired output size. If int, square crop
			is made.
	"""

	def __init__(self, output_size):
		assert isinstance(output_size, int)
		self.output_size = (output_size, output_size)


	def __call__(self, sample):
		image, question, answer = sample['image'], sample['question'],sample['answer']

		h, w = image.shape[:2]
		new_h, new_w = self.output_size

		top = np.random.randint(0, h - new_h)
		left = np.random.randint(0, w - new_w)

		image = image[top: top + new_h,
					  left: left + new_w]
		return {'image': image, 'question': question,'answer': answer}

class CenterCrop(object):
	"""Crop randomly the image in a sample.

	Args:
		output_size (tuple or int): Desired output size. If int, square crop
			is made.
	"""

	def __init__(self, output_size):
		assert isinstance(output_size, int)
		self.output_size = (output_size, output_size)


	def __call__(self, sample):
		image, question, answer = sample['image'], sample['question'],sample['answer']

		h, w = image.shape[:2]
		new_h, new_w = self.output_size

		top = (h - new_h)//2
		left = (w - new_w)//2

		image = image[top: top + new_h,
					  left: left + new_w]
		return {'image': image, 'question': question,'answer': answer}


class ToTensor(object):
	def __call__(self, sample):
		image, question,answer = sample['image'], sample['question'], sample['answer']
		if(len(image.shape)==2):
			a = np.zeros((image.shape[0],image.shape[0],3))
			for i in range(3):
				a[:,:,i] = image
			image = a
			# print('hai')
		image = image.transpose((2, 0, 1))
		return {'image': torch.from_numpy(image),
				'question': torch.from_numpy(question),'answer': torch.from_numpy(answer)}

class Normalize(object):
	def __init__(self,mean,std):
		self.mean = mean
		self.std = std

	def __call__(self, sample):
		image, question,answer = sample['image'], sample['question'],sample['answer']

		norm = transforms.Normalize(mean=self.mean,
							 std=self.std)
		image = norm(image)
		

		return {'image': image,
				'question':question,
				'answer':answer}

data_transform = transforms.Compose([
		Rescale(256),
		RandomCrop(224),
		ToTensor(),
		Normalize(mean=[0.485, 0.456, 0.406],
							 std=[0.229, 0.224, 0.225])
	])

data_transform1 = transforms.Compose([
		Rescale(256),
		CenterCrop(224),
		ToTensor(),
		Normalize(mean=[0.485, 0.456, 0.406],
							 std=[0.229, 0.224, 0.225])
	])

train_dataset = QuestionDataset(answer_json = 'v2_mscoco_train2014_annotations.json',
	question_json ='v2_OpenEnded_mscoco_train2014_questions.json',
	image_dir=root_data_dir+'train2014/COCO_train2014_', transform=data_transform)
val_dataset = QuestionDataset(answer_json = 'v2_mscoco_val2014_annotations.json',
	question_json ='v2_OpenEnded_mscoco_val2014_questions.json',
	image_dir=root_data_dir+'val2014/COCO_val2014_', transform=data_transform1)

trainloader = torch.utils.data.DataLoader(train_dataset,batch_size=128,shuffle =False,num_workers=4)


def sort_batch(data, seq_len):
	sorted_idx = sorted(enumerate(seq_len), key=lambda x:x[1],reverse = True)
	sorted_seq_len = [i[1] for i in sorted_idx]
	sorted_idx = [i[0] for i in sorted_idx]
	sorted_idx = torch.LongTensor(sorted_idx)
	# sorted_seq_len = seq_len[sorted_idx]
	data['question'] = data['question'][sorted_idx]
	data['image'] = data['image'][sorted_idx]
	data['answer'] = data['answer'][sorted_idx]
	return data, sorted_seq_len

def GetDataLoader():
	train_dataset = QuestionDataset(answer_json = 'v2_mscoco_train2014_annotations.json',
	question_json ='v2_OpenEnded_mscoco_train2014_questions.json',
	image_dir=root_data_dir+'train2014/COCO_train2014_', transform=data_transform)
	val_dataset = QuestionDataset(answer_json = 'v2_mscoco_val2014_annotations.json',
	question_json ='v2_OpenEnded_mscoco_val2014_questions.json',
	image_dir=root_data_dir+'val2014/COCO_val2014_', transform=data_transform1)

	trainloader = torch.utils.data.DataLoader(train_dataset,batch_size=128,shuffle =True,num_workers=4)
	valloader = torch.utils.data.DataLoader(val_dataset,batch_size=128,shuffle =True,num_workers=4)
	return trainloader, valloader, maxi, len(word_to_ix)


# for i_batch, sample_batched in enumerate(trainloader):
# 	print(i_batch)
# 	seq_len =[]
# 	for i in sample_batched['question']:
# 		seq_len.append(maxi-len(i[i==0]))
# 	sample_batched,seq_len = sort_batch(sample_batched,seq_len)
	
# 	print(i_batch, sample_batched['image'].size(),
# 		  sample_batched['question'].size(),
# 		  sample_batched['answer'].size())
# 	if(i_batch==3):
# 		print('hello')
		