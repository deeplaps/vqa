from __future__ import division
import os
import time
import copy
import torch
import torchvision
import pandas as pd
import numpy as np
from skimage import io, transform
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler as lsched
from torch.autograd import Variable
from torchvision import models, transforms
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pack_padded_sequence
import torch.nn.functional as F
import torch.nn.init as init
from data_conv import QuestionDataset, GetDataLoader, sort_batch

EMBEDDING_DIM = 300
HIDDEN_DIM = 512
K = 1000
maxi = 0

import warnings
warnings.filterwarnings("ignore")

def validation(model,dataLoader):
	model.eval()
	correct = 0
	total = 0
	for data in dataLoader:
		seq_len =[]
		for q in data['question']:
			seq_len.append(maxi-len(q[q==0]))
		data,seq_len = sort_batch(data,seq_len)
		# get the inputs
		img, query, ans = data['image'].type(torch.FloatTensor), data['question'].type(torch.LongTensor), data['answer'].type(torch.LongTensor)
		# get the inputs
		# img, query, ans = data

		# wrap them in Variable
		img, query,ans = Variable(img.cuda()),Variable(query.cuda()), ans.cuda()
		outputs = model(img,query,seq_len)
		_, predicted = torch.max(outputs.data, 1)
		# print (predicted.size())
		total += ans.size(0)
		correct += (predicted == ans[:,0]).sum()

	return (correct/total)


class LSTM_RNN(nn.Module):
	def __init__(self, embedding_dim, hidden_dim,num_hidden_layers, vocab_size):
		super(LSTM_RNN, self).__init__()
		self.hidden_dim = hidden_dim
		self.num_hidden_layers = num_hidden_layers
		self.word_embeddings = nn.Embedding(vocab_size, embedding_dim)
		self.lstm = nn.LSTM(embedding_dim, hidden_dim,num_hidden_layers,batch_first = True)
		self.linear = nn.Linear(2*self.hidden_dim*self.num_hidden_layers,1024)
		# self.hidden = self.init_hidden()

	def init_hidden(self,batch_size):
		return (Variable(torch.zeros(self.num_hidden_layers,batch_size,self.hidden_dim).cuda()),
				Variable(torch.zeros(self.num_hidden_layers,batch_size,self.hidden_dim).cuda()))

	def forward(self, sentence,seq_length):
		embeds = self.word_embeddings(sentence)
		packed_input = pack_padded_sequence(embeds,seq_length,batch_first=True)
		output = Variable(torch.zeros(1,2*self.hidden_dim*self.num_hidden_layers))
		h0 = Variable(torch.zeros(self.num_hidden_layers,(embeds.size())[0],self.hidden_dim).cuda())
		c0 = h0
		x,(hn,cn) = self.lstm(packed_input,(h0,c0))
		# length = embeds.size()[1]
		# for i in range(length):
		# 	self.hidden,(hn,cn) = self.lstm((embeds[:,i,:]).view(-1,EMBEDDING_DIM), self.hidden)
		# 	if(i==length-1):
		output = hn[0,:,:]
		for i in range(self.num_hidden_layers-1):
			output = torch.cat((output,hn[i+1,:,:]),1)
		for i in range(self.num_hidden_layers):
			output = torch.cat((output,cn[i,:,:]),1)
		output = self.linear(output)
		return output

class VQA_model(nn.Module):
	def __init__(self, embedding_dim, hidden_dim,num_hidden_layers, vocab_size):
		super(VQA_model, self).__init__()
		
		
		self.CNN =models.vgg16_bn(pretrained=False)
		self.CNN.load_state_dict(torch.load('vgg16_bn-6c64b313.pth'))
		for param in self.CNN.parameters():
			param.requires_grad = False
		# Making the last fc layer to be finetunable
		### If I is to be taken
		# self.CNN.classifier[6] = nn.Linear(4096, 1024)	

		### If norm-I is to be taken 
		# self.CNN.classifier[6] = nn.BatchNorm1d(4096)
		# self.fc_cnn = nn.Linear(4096,1024)

		# self.CNN.classifier = nn.Sequential(
  #           self.CNN.classifier[0],
  #           self.CNN.classifier[1],
  #           self.CNN.classifier[2],
  #           self.CNN.classifier[3],
  #           self.CNN.classifier[4],
  #           self.CNN.classifier[5],
  #           # nn.BatchNorm1d(4096),
  #           nn.Linear(4096,1024),
  #           )
		self.CNN.classifier = nn.Sequential(
            *(self.CNN.classifier[i] for i in range(6)))
		self.fc_cnn = nn.Linear(4096,1024)
		self.lstm = LSTM_RNN(embedding_dim, hidden_dim,num_hidden_layers,vocab_size)
		self.MLP = nn.Sequential(
			nn.Dropout(),
			nn.Linear(1024, 1000),
			# nn.ReLU(inplace=True),
			nn.Tanh(),
			nn.Dropout(),
			nn.Linear(1000, 1000),
			# nn.ReLU(inplace=True),
			nn.Tanh(),
			nn.Linear(1000, K+1),
		)


	def forward(self, image,sentence,seq_length):
		img_features = self.CNN(image)
		norm = img_features.norm(p=2, dim=0)
		img_features = img_features.div(norm.expand_as(img_features))
		img_features = self.fc_cnn(img_features)
		## For norm-I
		# img_features = self.fc_cnn(img_features)
		# batch_size = (sentences.size())[1]
		# self.LSTM.hidden = self.LSTM.init_hidden(batch_size)
		query_features = self.lstm(sentence,seq_length)
		# print(img_features.size())
		# print(query_features.size())
		final_emb = img_features*query_features
		output = self.MLP(final_emb)
		return output

def train(trainloader,valloader, num_hidden_layers,len_data_vocab):
	model = VQA_model(EMBEDDING_DIM, HIDDEN_DIM,num_hidden_layers,
								 len_data_vocab)
	model_optimal = VQA_model(EMBEDDING_DIM, HIDDEN_DIM,num_hidden_layers,
										 len_data_vocab)
	model = model.cuda()
	model_optimal = model_optimal.cuda()

	criterion = nn.CrossEntropyLoss()
	optimizer = optim.SGD(filter(lambda p: p.requires_grad, model.parameters()),
										 lr=0.01,weight_decay = 0.0005, momentum=0.9)
	scheduler = lsched.ReduceLROnPlateau(optimizer,  mode='min', factor=0.1, patience=2)

	#stopping criteria parameters
	best_acc = 0.0
	min_delta = 1e-5
	p = 10
	epoch = 0

	train_error = []
	# while epoch < p:
	for epoch in range(1):
		model.train()	
		for i, data in enumerate(trainloader, 0):

			seq_len =[]
			for q in data['question']:
				seq_len.append(maxi-len(q[q==0]))
			data,seq_len = sort_batch(data,seq_len)
			# get the inputs
			train_img, train_query, train_ans = data['image'].type(torch.FloatTensor), data['question'].type(torch.LongTensor), data['answer'].type(torch.LongTensor)

			# wrap them in Variable
			train_img, train_query, train_ans = Variable(train_img.cuda()),	Variable(train_query.cuda()),Variable(train_ans.cuda()) 

			# zero the parameter gradients
			optimizer.zero_grad()

			# forward + backward + optimize
			train_output = model(train_img, train_query,seq_len)
			# print (train_ans.size())
			loss = criterion(train_output, train_ans[:,0])
			loss.backward()
			optimizer.step()

		print('One epoch finished')
		# val_acc = validation(model,valloader)
		# train_acc = validation(model,trainloader)
		val_acc = 0.8
		train_acc = 0.6
		scheduler.step(1-val_acc)

		print('Accuracy of the network on the validation set: %.5f %% and train acc: %.5f' %
														 (100*val_acc,100*train_acc))
		
		train_error.append(train_acc)
		 

		# if (val_acc - best_acc) >= min_delta:
		# 	best_acc = val_acc
		# 	model_optimal.load_state_dict(model.state_dict())
		# 	epoch = 0
		# else:
		# 	epoch = epoch +1

	print (train_error)
	return model
	# return model_optimal


if __name__ == '__main__':
	
	trainloader,valloader,maxi, data_vocab_length = GetDataLoader()
	print ("data processes")
	model_start_time  = time.time()
	model = train(trainloader,valloader, 2,data_vocab_length)
	total_train = time.time()-model_start_time

	# val_start_time = time.time()
	# acc = validation(model,valloader)
	# total_val = time.time()- val_start_time
	# print('Accuracy of the network on the validation set: %f %%' % (100*acc))


	# torch.save(model.state_dict(),'./Models/lstm_model.pth')
	# print('Train time %0.7f and val time %0.7f' % (total_train/len(train_data),total_val/len(val_data)))

		